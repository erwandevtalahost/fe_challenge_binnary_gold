import { useState } from 'react';
import ProfilPage from './pages/Profile/ProfilePage';

import CarList from './pages/CarList';
import { Link } from "react-router-dom";

import CarDetail from './pages/CarDetail';
import Notfound from './pages/Notfound';

import './pages/styles.css';

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';

import Container from '@mui/material/Container';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { Bolt } from '@mui/icons-material';
import gambar from './assets/img_car.png';
import Home from './pages/Home';
import Pricing from './pages/Pricing';
import Test from './pages/Test';
import Banner from './pages/Banner';
import Dummy from './pages/Dummy';
import Pagenew from './pages/Pagenew'
import Footer from './pages/Footer';
import CariMobil from './pages/CariMobil';
import DetailMobil from './pages/DetailMobil';




const navItems = ['Our Services', 'Why Us', 'Testimonial', 'FAQ'];
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));


function App() {
  return (
    
   
    <BrowserRouter>
     <Pricing/>
  




     
      <Routes>
        <Route path='/' element={<Pricing />} />
        <Route path='/profil' element={<ProfilPage />} />
        {/* <Route path='/mobil' element={<CarList />} />
        <Route path='/mobildetail/:id' element={<CarDetail />} /> */}
        <Route path='/testing' element={<Test/>} />
        <Route path='/pagebaru' element={<Pagenew/>} />
        <Route path='/detailmobil/:id' element={<DetailMobil/>} />
        <Route path='/carimobil' element={<CariMobil/>} />
        <Route path='/banner' element={<Dummy/>} />
        <Route path='*' element={<Notfound />} />
      </Routes>

      <Footer/>
    </BrowserRouter>
    
  )
}

export default App;